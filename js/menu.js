
function checkHowManyWays(radioOneWay) {
	if(radioOneWay.value == "manyWays"){
		document.getElementsByClassName("percentageBlock")[0].style.visibility = "visible";
	} else {
		document.getElementsByClassName("percentageBlock")[0].style.visibility = "hidden";
	}
}

function gameModeOnChange(selectGameMode) {
	var selectedGameMode = selectGameMode[selectGameMode.selectedIndex].value;
	
	if(selectedGameMode == "optional"){
		document.getElementById("optionalContent").style.display= "grid";
		document.getElementById("generateButtonTopOptions").style.display= "none";
		document.getElementById("generateButtonTopOutOptions").style.display= "none";
	} else {
		document.getElementById("optionalContent").style.display= "none";
		document.getElementById("generateButtonTopOptions").style.display= "block";
		document.getElementById("generateButtonTopOutOptions").style.display= "block";
	}		
}

function applyToMaze(mazeController){
	var idOfContainer = document.getElementById("idOfContainerMaze1").value;
	var fieldSize =	document.getElementById("fieldSizeInput").value;
	var borderWidth = document.getElementById("borderWidthInput").value;
	var borderColor = "#" + document.getElementById("borderValue").value;
	var backgroundColor = "#" +  document.getElementById("backgroundValue").value;
	var backgroundImageElement = document.getElementById("backgroundTexture");
	var backgroundImage = backgroundImageElement[backgroundImageElement.selectedIndex].value;
	mazeController.setBorderColor(borderColor);
	Maze.createStylesAccordingParameters(idOfContainer, fieldSize, borderWidth, borderColor, backgroundImage, backgroundColor);
}