
function initialize(rows, columns, initialPosition, endPosition, optionalParameters) {
	mazeControl = new Maze(rows, columns, initialPosition, endPosition, optionalParameters);
}

class Maze {
	
	constructor(rows, columns, initialIndex, endIndex, optionalParameters) {
		this.mazeSquares = document.getElementsByClassName("mazeSquare");	
		this.rows = rows;
		this.columns = columns;
		this.vertices = rows * columns;
		this.initialIndex = initialIndex;
		this.endIndex = endIndex;
		this.optionalParameters = optionalParameters;
		this.imageElement;
		this.visibility = this.optionalParameters.getGameMode().getVisibility();
		this.functionOnKeyDown;
		this.borderColor = optionalParameters.getGameStyle().getBorderColor();
		this.borderColorRGB = w3color(this.borderColor).toRgbString();
		
		this.applyKeyListenerOnDiv(optionalParameters.getIdOfContainer());
		this.createBallAndInsertIt();
		
		Maze.setOtherBorderWidthAndBoxSize(this.optionalParameters);
		//this.removeKeyListenerOnDiv(optionalParameters.getIdOfContainer());
	}
	
	
	addImageElement(imageElement) { this.imageElement = imageElement; };
	getMazeSquares() { return this.mazeSquares; };
	getRows() { return this.rows; };
	getColumns() { return this.columns; };
	getImageElement() { return this.imageElement;};
	getInitialIndex() { return this.initialIndex; };
	setInitialIndex(initialIndex) {
		this.initialIndex = initialIndex;
		if(initialIndex == this.endIndex) {			
			alert("WINNER!"); 
		} 
	};
	
	setInitialIndexBlockToSee(initialIndex) {
		this.initialIndex = initialIndex;
		this.imageElement.parentNode.style.opacity = 1;
		this.inspectRightAndSetBorder(this.mazeSquares[this.initialIndex + 1]);
		this.inspectBottomAndSetBorder(this.mazeSquares[this.initialIndex + this.columns]);
		if(initialIndex == this.endIndex) {			
			alert("WINNER!"); 
		} 
	};
	
	setBorderColor(borderColor) {
		this.borderColor = borderColor;
		this.borderColorRGB = w3color(this.borderColor).toRgbString();
	}
	
	getVertices() { return this.vertices; };
	getOptionalParameters() { return this.optionalParameters; };
	removeInsertedImage() { if(this.imageElement.parentNode != null ) { this.imageElement.parentNode.removeChild(this.imageElement); } else { alert('not move');} };
	
	
	removeInsertedImageBlockToSee() {
		if(this.imageElement.parentNode != null ) {
			this.imageElement.parentNode.style.opacity = 0;
			this.inspectRightAndRemoveBorder(this.mazeSquares[this.initialIndex + 1]);
			this.inspectBottomAndRemoveBorder(this.mazeSquares[this.initialIndex + this.columns]);
			this.imageElement.parentNode.removeChild(this.imageElement);		
		} else { 
			alert('not move');
		} 
	};
	
	inspectRightAndSetBorder(nextSibling){
		if(this.imageElement.parentNode == null || typeof nextSibling !== 'object') {
			return false;
		}
		
		var colorRGB  = window.getComputedStyle(nextSibling,null).getPropertyValue('border-left-color');
		
		if( colorRGB == this.borderColorRGB) {
			this.imageElement.parentNode.style.borderRightColor = this.borderColor;
		} else {
			return false;
		}
	}
	
	inspectBottomAndSetBorder(nextSibling){
		if(this.imageElement.parentNode == null || typeof nextSibling !== 'object') {
			return false;
		}
		
		var colorRGB  = window.getComputedStyle(nextSibling,null).getPropertyValue('border-top-color');
		
		if( colorRGB == this.borderColorRGB) {
			this.imageElement.parentNode.style.borderBottomColor = this.borderColor;
		} else {
			return false;
		}
	}
	
	inspectRightAndRemoveBorder(nextSibling){
		if(this.imageElement.parentNode == null || typeof nextSibling !== 'object') {
			return false;
		}
		
		var colorRGB  = window.getComputedStyle(nextSibling,null).getPropertyValue('border-left-color');
		
		if( colorRGB == this.borderColorRGB) {
			this.imageElement.parentNode.style.borderRightColor = "transparent";
		} else {
			return false;
		}
	}
	
	inspectBottomAndRemoveBorder(nextSibling){
		if(this.imageElement.parentNode == null || typeof nextSibling !== 'object') {
			return false;
		}
		
		var colorRGB  = window.getComputedStyle(nextSibling,null).getPropertyValue('border-top-color');
		
		if( colorRGB == this.borderColorRGB) {
			this.imageElement.parentNode.style.borderBottomColor = "transparent";
		} else {
			return false;
		}
	}
			
	isWallUp() { 
		if(this.imageElement.parentNode == null) {
			console.log("Null parent!");
			return false;
		}
	
		var colorRGB  = window.getComputedStyle(this.imageElement.parentNode,null).getPropertyValue('border-top-color');
	
		if( colorRGB == this.borderColorRGB) {
			return true;
		} else {
			return false;
		}
	};
	
	isWallBottom(bottomElement) { 
		if(this.imageElement.parentNode == null) {
			console.log("Null parent!");
			return false;
		}
		
		var colorRGB  = window.getComputedStyle(bottomElement,null).getPropertyValue('border-top-color');
			
		if( colorRGB == this.borderColorRGB) {
			return true;
		} else {
			return false;
		}
	};
	
	isWallLeft() { 
		if(this.imageElement.parentNode == null) {
			console.log("Null parent!");
			return false;
		}
		
		var colorRGB  = window.getComputedStyle(this.imageElement.parentNode,null).getPropertyValue('border-left-color');
	
		if( colorRGB == this.borderColorRGB) {
			return true;
		} else {
			return false;
		}
	};
	
	isWallRight(nextSibling) { 
		if(this.imageElement.parentNode == null ) {
			console.log("Null parent!");
			return false;
		}
		
		var colorRGB  = window.getComputedStyle(nextSibling,null).getPropertyValue('border-left-color');
		
		//not cross border - border color is set
		if( colorRGB == this.borderColorRGB) {
			return true;
		} else {
			return false;
		}
	};
	
	createBallAndInsertIt() {
		var img = document.createElement("IMG");
		img.src = "pictures/icons/ball-svgrepo-com.svg";
		img.alt = "playable ball";
		img.classList.add("ball");
		
		
		if(this.getMazeSquares() == null) {
			alert("No maze availabel");
			return;
		}
		
		var placeToInsert = this.mazeSquares[0];
		placeToInsert.appendChild(img);
		this.addImageElement(img);
		
		if(this.visibility){
			this.setInitialIndex(0);
		} else {
			this.setInitialIndexBlockToSee(0);
		}
	}




	keyUpManger() {
		if(this.initialIndex - this.columns >= 0 && !this.isWallUp()) {
			var placeToInsert = this.mazeSquares[this.initialIndex - this.columns];
			this.removeInsertedImage();
			placeToInsert.appendChild(this.imageElement);
			this.setInitialIndex(this.initialIndex - this.columns);
		}
	}

	keyDownManger() {
		if(this.initialIndex + this.columns <= this.vertices && !this.isWallBottom(this.mazeSquares[this.initialIndex + this.columns])) {
			var placeToInsert = this.mazeSquares[this.initialIndex + this.columns];
			this.removeInsertedImage();
			placeToInsert.appendChild(this.imageElement);
			this.setInitialIndex(this.initialIndex + this.columns);
		
		}
	}

	keyLeftManger() {
		if(this.initialIndex % this.columns != 0 && !this.isWallLeft()) {
			var placeToInsert = this.mazeSquares[this.initialIndex - 1];
			this.removeInsertedImage();
			placeToInsert.appendChild(this.imageElement);
			this.setInitialIndex(this.initialIndex - 1);
		}
	}

	keyRightManger() {
		if((this.initialIndex + 1) % this.columns != 0 && !this.isWallRight(this.mazeSquares[this.initialIndex + 1])) {
			var placeToInsert = this.mazeSquares[this.initialIndex + 1];
			this.removeInsertedImage();
			placeToInsert.appendChild(this.imageElement);
			this.setInitialIndex(this.initialIndex + 1);
		}
	}

	
	keyUpMangerOptionalParameters() {
		if(this.initialIndex - this.columns >= 0 && !this.isWallUp()) {
			var placeToInsert = this.mazeSquares[this.initialIndex - this.columns];
			this.removeInsertedImageBlockToSee();
			placeToInsert.appendChild(this.imageElement);
			this.setInitialIndexBlockToSee(this.initialIndex - this.columns);
		}
	}

	keyDownMangerOptionalParameters() {
		if(this.initialIndex + this.columns <= this.vertices && !this.isWallBottom(this.mazeSquares[this.initialIndex + this.columns])) {
			var placeToInsert = this.mazeSquares[this.initialIndex + this.columns];
			this.removeInsertedImageBlockToSee();
			placeToInsert.appendChild(this.imageElement);
			this.setInitialIndexBlockToSee(this.initialIndex + this.columns);
		
		}
	}

	keyLeftMangerOptionalParameters() {
		if(this.initialIndex % this.columns != 0 && !this.isWallLeft()) {
			var placeToInsert = this.mazeSquares[this.initialIndex - 1];
			this.removeInsertedImageBlockToSee();
			placeToInsert.appendChild(this.imageElement);
			this.setInitialIndexBlockToSee(this.initialIndex - 1);
		}
	}

	keyRightMangerOptionalParameters() {
		if((this.initialIndex + 1) % this.columns != 0 && !this.isWallRight(this.mazeSquares[this.initialIndex + 1])) {
			var placeToInsert = this.mazeSquares[this.initialIndex + 1];
			this.removeInsertedImageBlockToSee();
			placeToInsert.appendChild(this.imageElement);
			this.setInitialIndexBlockToSee(this.initialIndex + 1);
		}
	}
	
	onKeyDown(e){
		
		if(this.visibility) {
			if(e.keyCode == '38'){
				this.keyUpManger();
			} 
		
			if(e.keyCode == '40'){
				this.keyDownManger();
			}
		
			if(e.keyCode == '37'){
				this.keyLeftManger();
			}
		
			if(e.keyCode == '39'){
				this.keyRightManger();
			}
			
		} else {
			
			if(e.keyCode == '38'){
				this.keyUpMangerOptionalParameters();
			} 
		
			if(e.keyCode == '40'){
				this.keyDownMangerOptionalParameters();
			}
		
			if(e.keyCode == '37'){
				this.keyLeftMangerOptionalParameters();
			}
		
			if(e.keyCode == '39'){
				this.keyRightMangerOptionalParameters();
			}
		}
		
		e.preventDefault();
		return false;
	};



	applyKeyListenerOnDiv(idOfDiv){
		this.functionOnKeyDown = this.onKeyDown.bind(this); //otherwise listener cant be removed
		document.getElementById(idOfDiv).addEventListener('keydown', this.functionOnKeyDown);
	}

	removeKeyListenerOnDiv(idOfDiv){
		document.getElementById(idOfDiv).removeEventListener('keydown',this.functionOnKeyDown);
	}
	
	static setOtherBorderWidthAndBoxSize(optionalParameters){
		var borderWidth = optionalParameters.getBorderWidth();
		
		Maze.createStylesAccordingParameters(optionalParameters.getIdOfContainer(), optionalParameters.getFieldSize(), borderWidth, optionalParameters.getGameStyle().getBorderColor(), optionalParameters.getGameStyle().getBackgroundTextureName(), optionalParameters.getGameStyle().getBackgroundColor());

	}
	
	static createStylesAccordingParameters(idOfContainer, fieldSize, borderWidth, borderColorNotConverted, backgroundImage, backgroundColor) {
		
		
		var style=document.createElement('style');
		var parentDiv = document.getElementById(idOfContainer);
		parentDiv.style.width = String(fieldSize * parentDiv.getAttribute('columns')) +"px";
		var ballSize = fieldSize - 2*borderWidth;
		
		var borderColor = w3color(borderColorNotConverted).toRgbString();
		var background;
		if(backgroundImage != "none"){
			background = 'background-image: url(pictures/backgroundStyle/'+ backgroundImage +'); ';
		} else {
			background = 'background-color: '+ backgroundColor +';\n background-image: none; ';
		}
		if(fieldSize > 10){
			style.type='text/css';
			if(style.styleSheet){
				style.styleSheet.cssText='.mazeSquare { '+
											'width: '+ fieldSize +'px; '+ 
											'height: '+ fieldSize +'px; '+
										'} '+
										'.borderLeftRightUpDown, .borderBottom, .borderUp, .borderRight, .borderLeft, '+
										'.borderUpBottom, .borderRightBottom, .borderLeftBottom, .borderRightUp, '+ 
										'.borderLeftUp, .borderLeftRight, .borderRightUpBottom, .borderLeftUpBottom, '+
										'.borderLeftRightBottom, .borderLeftRightUp, .noBorder { '+
											'border-top-width: '+borderWidth +'px;\n '+
											'border-bottom-width: '+borderWidth +'px;\n '+
											'border-left-width: '+borderWidth +'px;\n '+
											'border-right-width: '+borderWidth +'px;\n '+
										'}'+
										'\n '+
										'.ball {\n '+
											'width: '+ ballSize +'px;\n '+
											'height: '+ ballSize +'px;\n '+
											'margin-left: '+ (borderWidth/2) +'px;\n '+
											'margin-top: '+ (borderWidth/2) +'px;\n '+
										'}\n '+
										'.borderLeftRightUpDown { '+
											'border-color: '+ borderColor +' '+ borderColor +' '+ borderColor +' '+ borderColor +'; '+
										'}\n '+
										'.borderBottom { '+
											'border-color: transparent transparent '+ borderColor +' transparent;'+
										'}\n '+
										'.borderUp { '+
											'border-color: '+ borderColor +' transparent transparent  transparent;'+
										'}\n '+
										'.borderRight { '+
											'border-color: transparent '+ borderColor +' transparent transparent; '+
										'}\n '+
										'.borderLeft { '+
											'border-color: transparent transparent  transparent '+ borderColor +'; '+
										'}\n '+
										'.borderUpBottom { '+
											'border-color: '+ borderColor +' transparent '+ borderColor +' transparent; '+
										'}\n '+
										'.borderRightBottom { '+
											'border-color: transparent '+ borderColor +' '+ borderColor +' transparent; '+
										'}\n '+
										'.borderLeftBottom { '+
											'border-color: transparent  transparent '+ borderColor +' '+ borderColor +'; '+
										'}\n '+
										'.borderRightUp { '+
											'border-color: '+ borderColor +' '+ borderColor +' transparent transparent; '+
										'}\n '+
										'.borderLeftUp { '+
											'border-color: '+ borderColor +' transparent  transparent '+ borderColor +'; '+
										'}\n '+
										'.borderLeftRight { '+
											'border-color: transparent '+ borderColor +'  transparent '+ borderColor +'; '+
										'}\n '+
										'.borderRightUpBottom { '+
											'border-color: '+ borderColor +' '+ borderColor +' '+ borderColor +' transparent; '+
										'}\n '+
										'.borderLeftUpBottom { '+
											'border-color: '+ borderColor +' transparent '+ borderColor +' '+ borderColor +'; '+
										'}\n '+
										'.borderLeftRightBottom { '+
											'border-color: transparent '+ borderColor +' '+ borderColor +' '+ borderColor +'; '+
										'}\n '+
										'.borderLeftRightUp { '+
											'border-color: '+ borderColor +' '+ borderColor +' transparent '+ borderColor +'; '+
										'}\n '+
										'.borderBox { '+
											background +
										'}\n ';
			}else{
				style.appendChild(document.createTextNode('.mazeSquare { '+
												'width: '+ fieldSize +'px; '+ 
												'height: '+ fieldSize +'px; '+
											'} '+
											'.borderLeftRightUpDown, .borderBottom, .borderUp, .borderRight, .borderLeft, '+
											'.borderUpBottom, .borderRightBottom, .borderLeftBottom, .borderRightUp, '+ 
											'.borderLeftUp, .borderLeftRight, .borderRightUpBottom, .borderLeftUpBottom, '+
											'.borderLeftRightBottom, .borderLeftRightUp, .noBorder { '+
												'border-top-width: '+borderWidth +'px;\n '+
												'border-bottom-width: '+borderWidth +'px;\n '+
												'border-left-width: '+borderWidth +'px;\n '+
												'border-right-width: '+borderWidth +'px;\n } '+
												'\n '+
												'.ball {\n '+
													'width: '+ ballSize +'px;\n '+
													'height: '+ ballSize +'px;\n '+
													'margin-left: '+ (borderWidth/2) +'px;\n '+
													'margin-top: '+ (borderWidth/2) +'px;\n '+
												'} '+
												'.borderLeftRightUpDown { '+
												'border-color: '+ borderColor +' '+ borderColor +' '+ borderColor +' '+ borderColor +'; '+
											'}\n '+
											'.borderBottom { '+
												'border-color: transparent transparent '+ borderColor +' transparent;'+
											'}\n '+
											'.borderUp { '+
												'border-color: '+ borderColor +' transparent transparent  transparent;'+
											'}\n '+
											'.borderRight { '+
												'border-color: transparent '+ borderColor +' transparent transparent; '+
											'}\n '+
											'.borderLeft { '+
												'border-color: transparent transparent  transparent '+ borderColor +'; '+
											'}\n '+
											'.borderUpBottom { '+
												'border-color: '+ borderColor +' transparent '+ borderColor +' transparent; '+
											'}\n '+
											'.borderRightBottom { '+
												'border-color: transparent '+ borderColor +' '+ borderColor +' transparent; '+
											'}\n '+
											'.borderLeftBottom { '+
												'border-color: transparent  transparent '+ borderColor +' '+ borderColor +'; '+
											'}\n '+
											'.borderRightUp { '+
												'border-color: '+ borderColor +' '+ borderColor +' transparent transparent; '+
											'}\n '+
											'.borderLeftUp { '+
												'border-color: '+ borderColor +' transparent  transparent '+ borderColor +'; '+
											'}\n '+
											'.borderLeftRight { '+
												'border-color: transparent '+ borderColor +'  transparent '+ borderColor +'; '+
											'}\n '+
											'.borderRightUpBottom { '+
												'border-color: '+ borderColor +' '+ borderColor +' '+ borderColor +' transparent; '+
											'}\n '+
											'.borderLeftUpBottom { '+
												'border-color: '+ borderColor +' transparent '+ borderColor +' '+ borderColor +'; '+
											'}\n '+
											'.borderLeftRightBottom { '+
												'border-color: transparent '+ borderColor +' '+ borderColor +' '+ borderColor +'; '+
											'}\n '+
											'.borderLeftRightUp { '+
												'border-color: '+ borderColor +' '+ borderColor +' transparent '+ borderColor +'; '+
											'}\n '+
											'.borderBox { '+
												background+
											'}\n '));
			}
			document.getElementsByTagName('head')[0].appendChild(style);
		}
	}
}
