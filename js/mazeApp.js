
function test(mazeController) {
	var gameMode1 = new GameMode("easy", true, 2, true, 0.25, -1);
	var gameStyle1 = new GameStyle(true, "red", "red", "grass1.bmp");
	var optionalParameters1 = new OptionalParameters(gameMode1, 50, "5px", gameStyle1, "novy");
	var maze3 = new MazeGenerator(10, 10, optionalParameters1);
	document.getElementById("idOfContainerMaze1").value= optionalParameters1.getIdOfContainer();
	mazeController = new Maze(10, 10, 1, 89, optionalParameters1);
	
	return mazeController;
}


function mazeToPlayManager(mazeController) {

	var mazeHeight = parseInt(document.getElementById('mazeWidth').value);
	var mazeWidth = parseInt(document.getElementById('mazeHeight').value);
	var initialPosition = 1;
	var winningPosition = mazeHeight * mazeWidth - mazeHeight -1;
	
	var gameModeText = document.getElementById('gameModeText').value;
	var visibility = (document.getElementById('visibility').checked)? true : false;
	var numberVisibleSteps = 2;
	var oneWay = (document.getElementById('oneWayRadio').checked)? true : false;
	var probabilityManyWays = (oneWay == false)?  parseFloat(document.getElementById('probabilityInput').value) : 0.0;
	var timeLimit = -1;
	
	//console.log("Game mode text: "+gameModeText);
	//console.log("Visibility: "+visibility);
	//console.log("Number visibe steps: "+numberVisibleSteps);
	//console.log("One way mode: "+oneWay);
	//console.log("Probability many ways: "+probabilityManyWays);
	var gameMode1 = new GameMode(gameModeText, visibility, numberVisibleSteps, oneWay, probabilityManyWays, timeLimit);
	
	
	var effects = (document.getElementById('effects').checked)? true : false;
	var borderColor = "#" + document.getElementById("borderValue").value;
	var backgroundColor = "#" +  document.getElementById("backgroundValue").value;
	var backgroundImageElement = document.getElementById("backgroundTexture");
	var backgroundTextureName = backgroundImageElement[backgroundImageElement.selectedIndex].value;
	
	var gameStyle1 = new GameStyle(effects, borderColor, backgroundColor, backgroundTextureName);
	
	
	
	var idOfContainer = document.getElementById("idOfContainerMaze1").value;

	var nonEmpty = document.getElementById(idOfContainer);

	if( nonEmpty != null){
		nonEmpty.innerHTML = "";
		nonEmpty.remove();
	}
	mazeController.setBorderColor(borderColor);
	var fieldSize =	parseInt(document.getElementById("fieldSizeInput").value);
	var borderWidth = parseInt(document.getElementById("borderWidthInput").value);
	
	var optionalParameters1 = new OptionalParameters(gameMode1, fieldSize , String(borderWidth) + "px", gameStyle1, idOfContainer);
	
	//console.log("Width: "+mazeWidth);
	//console.log("Height: "+mazeHeight);
	var mazeGenerate5 = new MazeGenerator(mazeWidth, mazeHeight, optionalParameters1);
	
	document.getElementById("idOfContainerMaze1").value= optionalParameters1.getIdOfContainer();
	mazeController = new Maze(mazeWidth, mazeHeight, initialPosition, winningPosition, optionalParameters1);

	mazeController.setBorderColor(borderColor);
	return mazeController;
}