
	
class RandomPermutation {
	
	constructor(permutationLength) {
		this.randomPermutation;
		this.permutationLength = permutationLength;
		
		this.initialyzePermutation(permutationLength);
		this.createRandomPermutation(permutationLength);
	}
	
	getPermutationFromPosition(position) {  return this.randomPermutation[position]; };
	
	getRandomArbitrary(min, max) {
		return Math.floor(Math.random() * (max - min) + min);
	}

	initialyzePermutation(permutationLength) {
		this.randomPermutation = new Array();
		this.randomPermutation.push(0);
	
		for(var i=1; i<= permutationLength; i++){
			this.randomPermutation.push(i);
		}
	}
	
	createRandomPermutation(permutationLength) {
		var toHelp, randomNumberInRange;
		for(var i=1; i<= permutationLength; i++){
			randomNumberInRange = this.getRandomArbitrary(i, permutationLength);
			toHelp = this.randomPermutation[randomNumberInRange];
			this.randomPermutation[randomNumberInRange] = this.randomPermutation[i];
			this.randomPermutation[i] = toHelp;
		}
	}
	
}

class RowsThanColumns{
	
	constructor(rows, columns, vertices) {
		this.maxRows = rows;
		this.maxColumns = columns;
		this.vertices = vertices;
		this.bound = this.vertices - this.maxRows;
		
		this.constantToMove = this.vertices - this.maxRows;
		this.firstColumnEdgeStart = this.bound + 1;
		this.firstColumnEdgeEnd = this.bound + this.maxColumns*(this.maxRows - 2) + 1;
		
		this.lastColumnEdgeStart = this.bound + this.maxColumns;
		this.lastColumnEdgeEnd = this.bound + this.maxColumns*( this.maxRows - 1);
		
		this.firstRowEdgeStart = 1;
		this.numberEdgeInRow = this.firstRowEdgeEnd = this.maxColumns - 1;
		this.columnStep = this.maxColumns;
		this.lastRowEdgeStart = this.firstColumnEdgeStart - this.numberEdgeInRow;
		this.lastRowEdgeEnd = this.bound;
	}
	
	applyFunction(connectionIdentifier) {
		var actualRows;
		
		if(connectionIdentifier <= this.bound) {
			actualRows = Math.floor(connectionIdentifier / (this.maxColumns - 1)) - ((connectionIdentifier % (this.maxColumns - 1) == 0)? 1 : 0);
			return new VerticePair(connectionIdentifier + actualRows, connectionIdentifier + actualRows + 1);
		} else {
			
			return new VerticePair(connectionIdentifier - this.constantToMove, connectionIdentifier - this.constantToMove + this.maxColumns);
		}
	}
	
	applyInverseFunction(vertice) {
		this.edgeOneLeft = vertice- Math.floor(vertice/this.maxColumns) -1;
		this.edgeTwoRight;
		
		if(vertice % this.maxColumns != 0){
			this.edgeTwoRight = this.edgeOneLeft + 1;
		} else {
			this.edgeTwoRight = -1;
		}
		
		if(vertice % this.maxColumns == 1){
			this.edgeOneLeft = -1;
		}
		
		this.edgeFourBottom = vertice + this.constantToMove;
		
		this.edgeThreeUp  = this.edgeFourBottom - this.maxColumns;
		if(vertice > this.vertices - this.maxColumns) {
			this.edgeFourBottom = -1;
		}
		
		if(vertice <= this.maxColumns) {
			this.edgeThreeUp = -1;
		}
		
		return new VerticeQuatro(this.edgeFourBottom, -1, this.edgeTwoRight, -1);
	}
	
	getFirstRowEdgeStart(){ return this.firstRowEdgeStart; }

	getFirstRowEdgeEnd() { return this.firstRowEdgeEnd; }
	
	getLastRowEdgeStart() { return this.lastRowEdgeStart; }
	
	getLastRowEdgeEnd() { return this.lastRowEdgeEnd; }
	
	getNumberEdgesInRow() { return this.numberEdgeInRow; }
	
	getFirstColumnEdgeStart() { return this.firstColumnEdgeStart; }
	
	getFirstColumnEdgeEnd() { return this.firstColumnEdgeEnd; }
	
	getLastColumnEdgeStart() { return this.lastColumnEdgeStart; }
	
	getLastColumnEdgeEnd() { return this.lastColumnEdgeEnd; }
	
	getColumnStep() { return this.columnStep; }
	
	getRowStep() { return 1; }
	
}

class VerticePair {
	
	constructor(x, y) {
		this.x = x;
		this.y = y;
	}
	
	getX() { return this.x; }
	getY() { return this.y; }	
}

class VerticeQuatro {
	
	constructor(edgeOneLeft, edgeTwoRight, edgeThreeUp, edgeFourBottom) {
		this.edgeOneLeft = edgeOneLeft;
		this.edgeTwoRight = edgeTwoRight;
		this.edgeThreeUp = edgeThreeUp;
		this.edgeFourBottom = edgeFourBottom;
	}
	
	getIndexEdgeOnLeft(){ return this.edgeOneLeft; }
	getIndexEdgeOnRight(){ return this.edgeTwoRight; }
    getIndexEdgeOnUp(){ return this.edgeThreeUp; }
    getIndexEdgeOnBottom(){ return this.edgeFourBottom; }
	
	edgeOnLeft(decision){
		if(decision == false){
			this.edgeOneLeft = -1;
		} 
	}
	
	edgeOnRight(decision){
		if(decision == false){
			this.edgeTwoRight = -1;
		} 
	}
	
	edgeOnUp(decision){
		if(decision == false){
			this.edgeThreeUp = -1;
		} 
	}
	
	edgeOnBottom(decision){
		if(decision == false){
			this.edgeFourBottom = -1;
		}
	}
	
	classify(){
	  
		//ALL WALLS
		if(this.edgeOneLeft != -1 && this.edgeTwoRight != -1 && this.edgeThreeUp!= -1 && this.edgeFourBottom != -1) {
			this.category = 1;
		
		//ONLY THREE WALLS
		} else if(this.edgeOneLeft == -1 && this.edgeTwoRight == -1 && this.edgeThreeUp== -1 && this.edgeFourBottom != -1) {
			this.category = 2;
		} else if(this.edgeOneLeft == -1 && this.edgeTwoRight == -1 && this.edgeThreeUp!= -1 && this.edgeFourBottom == -1) {
			this.category = 3;
		} else if(this.edgeOneLeft == -1 && this.edgeTwoRight != -1 && this.edgeThreeUp== -1 && this.edgeFourBottom == -1) {
			this.category = 4;
		} else if(this.edgeOneLeft != -1 && this.edgeTwoRight == -1 && this.edgeThreeUp== -1 && this.edgeFourBottom == -1) {
			this.category = 5;
			
		//ONLY TWO WALLS
		} else if(this.edgeOneLeft == -1 && this.edgeTwoRight == -1 && this.edgeThreeUp!= -1 && this.edgeFourBottom != -1) {
			this.category = 6;
		} else if(this.edgeOneLeft == -1 && this.edgeTwoRight != -1 && this.edgeThreeUp== -1 && this.edgeFourBottom != -1) {
			this.category = 7;
		} else if(this.edgeOneLeft != -1 && this.edgeTwoRight == -1 && this.edgeThreeUp== -1 && this.edgeFourBottom != -1) {
			this.category = 8;
		} else if(this.edgeOneLeft == -1 && this.edgeTwoRight != -1 && this.edgeThreeUp!= -1 && this.edgeFourBottom == -1) {
			this.category = 9;
		} else if(this.edgeOneLeft != -1 && this.edgeTwoRight == -1 && this.edgeThreeUp!= -1 && this.edgeFourBottom == -1) {
			this.category = 10;
		} else if(this.edgeOneLeft != -1 && this.edgeTwoRight != -1 && this.edgeThreeUp== -1 && this.edgeFourBottom == -1) {
			this.category = 11;
			
		//ONLY ONE WALL
		} else if(this.edgeOneLeft == -1 && this.edgeTwoRight != -1 && this.edgeThreeUp!= -1 && this.edgeFourBottom != -1) {
			this.category = 12;
		} else if(this.edgeOneLeft != -1 && this.edgeTwoRight == -1 && this.edgeThreeUp!= -1 && this.edgeFourBottom != -1) {
			this.category = 13;
		} else if(this.edgeOneLeft != -1 && this.edgeTwoRight != -1 && this.edgeThreeUp== -1 && this.edgeFourBottom != -1) {
			this.category = 14;
		} else if(this.edgeOneLeft != -1 && this.edgeTwoRight != -1 && this.edgeThreeUp!= -1 && this.edgeFourBottom == -1) {
			this.category = 15;
			
		//NO WALLS
		} else if(this.edgeOneLeft == -1 && this.edgeTwoRight == -1 && this.edgeThreeUp== -1 && this.edgeFourBottom == -1) {
			this.category = 16;
		}
	}
	
	getClassOfWEBRepresentation() {
		if(this.category == 1) {
			return "mazeSquare borderLeftRightUpDown";
		} else if( this.category == 2) {
			return "mazeSquare borderBottom";
		} else if(this.category == 3) {
			return "mazeSquare borderUp";
		} else if( this.category == 4) {
			return "mazeSquare borderRight";
		} else if( this.category == 5) {
			return "mazeSquare borderLeft";
		} else if( this.category == 6) {
			return "mazeSquare borderUpBottom";
		} else if( this.category == 7) {
			return "mazeSquare borderRightBottom";
		} else if( this.category == 8) {
			return "mazeSquare borderLeftBottom";
		} else if( this.category == 9) {
			return "mazeSquare borderRightUp";
		} else if( this.category == 10) {
			return "mazeSquare borderLeftUp";
		} else if( this.category == 11) {
			return "mazeSquare borderLeftRight";
		} else if( this.category == 12) {
			return "mazeSquare borderRightUpBottom";
		} else if( this.category == 13) {
			return "mazeSquare borderLeftUpBottom";
		} else if( this.category == 14) {
			return "mazeSquare borderLeftRightBottom";
		} else if( this.category == 15) {
			return "mazeSquare borderLeftRightUp";
		} else if( this.category == 16) {
			return "mazeSquare noBorder";
		}
		
		return "unknown";
	}
}

class SetItem {
	
	constructor(value) {
		this.parent = this;
		this.value = value;
	}
    
    static find(itemFirst, itemSecond){ 
        var item1 = itemFirst;
        var item2 = itemSecond;
        if(item1.parent == item2.parent){
            return item1.parent;
          }
          
        while(item1 != item1.parent){
            item1 = item1.parent;
        }
          
        while(item2 != item2.parent){
            item2 = item2.parent;
        }
          
          
        if(item1.parent == item2.parent){
			return item1.parent;
        } else {
            return null;
        }     
    }
    
    static union(item1, item2){
        var toHelp;
        
        while(item1 != item1.parent){
             item1 = item1.parent;
          }
          
        while(item2 != item2.parent){
             toHelp = item2.parent;
             item2.parent = item1;
             item2 = toHelp;
          }
                
        item2.parent = item1.parent;
    }
 
}



class MazeGenerator {
	
	constructor(rows, columns, optionalParameters) {
		this.rows = rows;
		this.columns = columns;
		this.vertices = rows * columns;
		this.edges = (rows - 1) * columns + (columns - 1) * rows;
		
		this.edgesWalls;
		this.indexSetField = [];
		this.randomPermutation = new RandomPermutation(this.edges);
		this.mappingFunction = new RowsThanColumns(this.rows, this.columns, this.vertices);
		if( optionalParameters.getGameMode().getOneWay() == true ){
			this.generateMaze(this.edges, this.vertices, this.randomPermutation, this.mappingFunction);
		} else {
			this.generateMazeManyWays(this.edges, this.vertices, this.randomPermutation, this.mappingFunction, optionalParameters.getGameMode());
		}
		this.saveAsHTMLFile(this.rows, this.columns, this.vertices, this.mappingFunction, this.edgesWalls, optionalParameters);
	}
	
		
	generateMaze(edges, vertices, randomPermutation, mappingFunction) {
		this.edgesWalls = new Set();
		this.initializeIndexSetField(vertices);
		this.addBoundary(edges, mappingFunction);
		this.chooseEdgesAsWalls(edges, randomPermutation, mappingFunction);
	}
	
	generateMazeManyWays(edges, vertices, randomPermutation, mappingFunction, gameMode ) {
		this.edgesWalls = new Set();
		this.initializeIndexSetField(vertices);
		this.addBoundary(edges, mappingFunction);
		this.chooseEdgesAsWallsManyWays(edges, randomPermutation, mappingFunction, gameMode);
	}
	
	initializeIndexSetField(vertices) {
		this.indexSetField = [];
		for(var i=0; i<= vertices; i++){
		   this.indexSetField.push(null); 
		}
	}
	
	addBoundary(edges, mappingFunction) {
		var newSetItem1, newSetItem2;
		var vertice
		//BOUND FROM LEFT
		for(var i=mappingFunction.getFirstRowEdgeStart(); i <= mappingFunction.getFirstRowEdgeEnd(); i++){
			vertice = mappingFunction.applyFunction(i);
			if(this.indexSetField[vertice.getX()] == null){
				this.indexSetField[vertice.getX()] = new SetItem(vertice.getX());
				
			}
			if(this.indexSetField[vertice.getY()] == null){
				this.indexSetField[vertice.getY()] = new SetItem(vertice.getY());
				
			}
			SetItem.union(this.indexSetField[vertice.getX()],this.indexSetField[vertice.getY()]);
			this.edgesWalls.add(i);
		}
		
		//BOUND FTROM BOTTOM
		for(var i=mappingFunction.getLastRowEdgeStart(); i <= mappingFunction.getLastRowEdgeEnd(); i++){
			vertice = mappingFunction.applyFunction(i);
			if(this.indexSetField[vertice.getX()] == null){
				this.indexSetField[vertice.getX()] = new SetItem(vertice.getX());
			}
			if(this.indexSetField[vertice.getY()] == null){
				this.indexSetField[vertice.getY()] = new SetItem(vertice.getY());
			}
			SetItem.union(this.indexSetField[vertice.getX()],this.indexSetField[vertice.getY()]);
			this.edgesWalls.add(i);
		}
		
		//BOUND FROM LEFT
		for(var i=mappingFunction.getFirstColumnEdgeStart(); i <= mappingFunction.getFirstColumnEdgeEnd(); i=i + mappingFunction.getColumnStep()){
			vertice = mappingFunction.applyFunction(i);
			if(this.indexSetField[vertice.getX()] == null){
				this.indexSetField[vertice.getX()] = new SetItem(vertice.getX());
			}
			if(this.indexSetField[vertice.getY()] == null){
				this.indexSetField[vertice.getY()] = new SetItem(vertice.getY());
			}
			SetItem.union(this.indexSetField[vertice.getX()],this.indexSetField[vertice.getY()]);
			this.edgesWalls.add(i);
		}
		
		//BOUND FROM RIGHT
		for(var i=mappingFunction.getLastColumnEdgeStart(); i < mappingFunction.getLastColumnEdgeEnd(); i=i + mappingFunction.getColumnStep()){
			vertice = mappingFunction.applyFunction(i);
			if(this.indexSetField[vertice.getX()] == null){
				this.indexSetField[vertice.getX()] = new SetItem(vertice.getX());
			}
			if(this.indexSetField[vertice.getY()] == null){
				this.indexSetField[vertice.getY()] = new SetItem(vertice.getY());
			}
			SetItem.union(this.indexSetField[vertice.getX()],this.indexSetField[vertice.getY()]);
			this.edgesWalls.add(i);
		}
	}
	
	chooseEdgesAsWalls(edges, randomPermutation, mappingFunction) {
		var vertice;
		var connection;
		for(var i=1; i<= edges; i++) {
			connection = randomPermutation.getPermutationFromPosition(i);
			vertice = mappingFunction.applyFunction(connection);
			//console.log("Edge: "+connection + "    ("+vertice.getX() +" , "+vertice.getY() +")");
			this.veryfySetIndexesAndChooseWalls(connection, vertice);
		}  
	}
	
	chooseEdgesAsWallsManyWays(edges, randomPermutation, mappingFunction, gameMode) {
		var vertice;
		var connection;
		for(var i=1; i<= edges; i++) {
			connection = randomPermutation.getPermutationFromPosition(i);
			vertice = mappingFunction.applyFunction(connection);
			//console.log("Edge: "+connection + "    ("+vertice.getX() +" , "+vertice.getY() +")");
			this.veryfySetIndexesAndChooseWallsManyWays(connection, vertice, gameMode);
		}  
	}
	
	veryfySetIndexesAndChooseWalls(connection, vertice) {
		var setIndex1, setIndex2, parentSetIndex1, parentSetIndex2;
		var first, second;
		
		setIndex1 = this.indexSetField[vertice.getX()];  
		setIndex2 = this.indexSetField[vertice.getY()]; 
	
		if( setIndex1 == null && setIndex2 == null){ 
			this.indexSetField[vertice.getX()] =  new SetItem(vertice.getX());
			this.indexSetField[vertice.getY()] = new SetItem(vertice.getY());
			SetItem.union(this.indexSetField[vertice.getX()],this.indexSetField[vertice.getY()]);
			this.edgesWalls.add(connection);
				 
		} else if (setIndex1 == null) {
			
			first = new SetItem(vertice.getX());
			this.indexSetField[vertice.getX()] = first;
			SetItem.union(this.indexSetField[vertice.getY()],first);
			this.edgesWalls.add(connection);
		   
			
		} else if (setIndex2 == null ) {
			
			second = new SetItem(vertice.getY());
			this.indexSetField[vertice.getY()] = second; 
			SetItem.union(this.indexSetField[vertice.getX()],second);    
			this.edgesWalls.add(connection);
			  
			
		}  else if(SetItem.find(this.indexSetField[vertice.getX()],this.indexSetField[vertice.getY()]) == null){
			
			SetItem.union(this.indexSetField[vertice.getX()],this.indexSetField[vertice.getY()]);
	
			this.edgesWalls.add(connection);
				
		}  
	}
	
	veryfySetIndexesAndChooseWallsManyWays(connection, vertice, gameMode) {
		var setIndex1, setIndex2, parentSetIndex1, parentSetIndex2;
		var first, second;
		var probabilityOfManyWays = gameMode.getProbabilityManyWays();
		
		setIndex1 = this.indexSetField[vertice.getX()];   
		setIndex2 = this.indexSetField[vertice.getY()]; 
		
		if( setIndex1 == null && setIndex2 == null){ 
			this.indexSetField[vertice.getX()] =  new SetItem(vertice.getX());
			this.indexSetField[vertice.getY()] = new SetItem(vertice.getY());
			SetItem.union(this.indexSetField[vertice.getX()],this.indexSetField[vertice.getY()]);
			this.edgesWalls.add(connection);
				 
		} else if (setIndex1 == null) {
			
			first = new SetItem(vertice.getX());
			this.indexSetField[vertice.getX()] = first;
			SetItem.union(this.indexSetField[vertice.getY()],first);
			this.edgesWalls.add(connection);
		   
			
		} else if (setIndex2 == null ) {
			
			second = new SetItem(vertice.getY());
			this.indexSetField[vertice.getY()] = second; 
			SetItem.union(this.indexSetField[vertice.getX()],second);    
			this.edgesWalls.add(connection);
			
			//printInfo("INDEX BEGIN2: ",oldContainer);     
			
		}  else if(probabilityOfManyWays <= Math.random() && SetItem.find(this.indexSetField[vertice.getX()],this.indexSetField[vertice.getY()]) == null){
			
			//parentSetIndex1.hangToAnother(parentSetIndex2);
			SetItem.union(this.indexSetField[vertice.getX()],this.indexSetField[vertice.getY()]);
	
			this.edgesWalls.add(connection);
				
		}  
	}
	
	saveAsHTMLFile(rows, columns, vertices, mappingFunction, edgesWalls, optionalParameters){
		var htmlMaze = new HTMLMaze(rows, columns, vertices, mappingFunction, edgesWalls, optionalParameters);
	}
	
}



class HTMLMaze {
	
	constructor(rows, columns, vertices, mappingFunction, edgesWalls, optionalParameters){
		
		var fieldWidth = optionalParameters.getFieldSize();
		var parentDiv = document.createElement("DIV");
		var borderColor = optionalParameters.getGameStyle().getBorderColor();
		var backgroundTextureName = optionalParameters.getGameStyle().getBackgroundTextureName();
		parentDiv.id = optionalParameters.getIdOfContainer();
		parentDiv.setAttribute("tabindex","1");
		parentDiv.setAttribute("columns",columns);
		parentDiv.style.width = String(columns * fieldWidth) + "px";
		parentDiv.classList.add('mazeArea');
		
		if(optionalParameters.getGameStyle().getEffects() == true){
			parentDiv.style.borderColor = borderColor;
			if(backgroundTextureName == null){
				parentDiv.style.backgroundImage = "url(pictures/backgroundStyle/"+backgroundTextureName+")";
			} else {
				parentDiv.style.backgroundColor = optionalParameters.getGameStyle().getBackgroundColor();
			}
		}
		if(optionalParameters.getGameMode().getVisibility() == false){
			this.setWallsAccordingFunctionAndHideContent(parentDiv, vertices, mappingFunction, edgesWalls, optionalParameters);
		} else {
			this.setWallsAccordingFunction(parentDiv, vertices, mappingFunction, edgesWalls, optionalParameters);
		}
	
		document.getElementsByTagName("main")[0].appendChild(parentDiv);
	
	}
	
	setWallsAccordingFunctionAndHideContent(parentDiv, vertices, mappingFunction, edgesWalls, optionalParameters) {
		var possibleEdgesIndexes;
		var leftEdgeIndex, rightEdgeIndex, upEdgeIndex, bottomEdgeIndex;
		
		for(var i=1; i<= vertices; i++){
		   possibleEdgesIndexes = mappingFunction.applyInverseFunction(i);
		   
		   leftEdgeIndex = possibleEdgesIndexes.getIndexEdgeOnLeft();
		   if(leftEdgeIndex == -1 || !edgesWalls.has(leftEdgeIndex)){
			   possibleEdgesIndexes.edgeOnLeft(false);
		   }
		   
		   rightEdgeIndex = possibleEdgesIndexes.getIndexEdgeOnRight();
		   if(rightEdgeIndex == -1 || !edgesWalls.has(rightEdgeIndex)){
			   possibleEdgesIndexes.edgeOnRight(false);
		   }
		   
		   upEdgeIndex = possibleEdgesIndexes.getIndexEdgeOnUp();
		   if(upEdgeIndex == -1 || !edgesWalls.has(upEdgeIndex)){
			   possibleEdgesIndexes.edgeOnUp(false);
		   }
		   
		   bottomEdgeIndex = possibleEdgesIndexes.getIndexEdgeOnBottom();
		   if(bottomEdgeIndex != -1 && !edgesWalls.has(bottomEdgeIndex)){
			   possibleEdgesIndexes.edgeOnBottom(false);
		   }
		   
		   //console.log("i: "+ i +" -> ("+leftEdgeIndex+", "+rightEdgeIndex +" , "+ upEdgeIndex +" , "+ bottomEdgeIndex +") ");
		   possibleEdgesIndexes.classify();
		   
		   //this.outputDiv(possibleEdgesIndexes.getClassOfWEBRepresentation());
		   this.createInsertAndHideDiv(parentDiv, possibleEdgesIndexes.getClassOfWEBRepresentation(), optionalParameters); 
		}
	}
	
	setWallsAccordingFunction(parentDiv, vertices, mappingFunction, edgesWalls, optionalParameters) {
		var possibleEdgesIndexes;
		var leftEdgeIndex, rightEdgeIndex, upEdgeIndex, bottomEdgeIndex;
		
		for(var i=1; i<= vertices; i++){
		   possibleEdgesIndexes = mappingFunction.applyInverseFunction(i);
		   
		   leftEdgeIndex = possibleEdgesIndexes.getIndexEdgeOnLeft();
		   if(leftEdgeIndex == -1 || !edgesWalls.has(leftEdgeIndex)){
			   possibleEdgesIndexes.edgeOnLeft(false);
		   }
		   
		   rightEdgeIndex = possibleEdgesIndexes.getIndexEdgeOnRight();
		   if(rightEdgeIndex == -1 || !edgesWalls.has(rightEdgeIndex)){
			   possibleEdgesIndexes.edgeOnRight(false);
		   }
		   
		   upEdgeIndex = possibleEdgesIndexes.getIndexEdgeOnUp();
		   if(upEdgeIndex == -1 || !edgesWalls.has(upEdgeIndex)){
			   possibleEdgesIndexes.edgeOnUp(false);
		   }
		   
		   bottomEdgeIndex = possibleEdgesIndexes.getIndexEdgeOnBottom();
		   if(bottomEdgeIndex != -1 && !edgesWalls.has(bottomEdgeIndex)){
			   possibleEdgesIndexes.edgeOnBottom(false);
		   }
		   
		   //console.log("i: "+ i +" -> ("+leftEdgeIndex+", "+rightEdgeIndex +" , "+ upEdgeIndex +" , "+ bottomEdgeIndex +") ");
		   possibleEdgesIndexes.classify();
		   
		   //this.outputDiv(possibleEdgesIndexes.getClassOfWEBRepresentation());
		   this.createAndInsertDiv(parentDiv, possibleEdgesIndexes.getClassOfWEBRepresentation(),optionalParameters); 
		}
	}
	
	
	outputDiv(classOfDiv){
        console.log("<div class=\""+classOfDiv+"\">");
        console.log("</div>");
    }
	
	createAndInsertDiv(parentDiv, classOfDiv, optionalParameters){
		var newDiv = document.createElement("DIV");
		var arrayOfClasses = classOfDiv.split(" ");
		for(var i=0; i<arrayOfClasses.length; i++){
			newDiv.classList.add(arrayOfClasses[i]);
		}
		
		//newDiv.classList.add("mazeBlock");
		newDiv.classList.add("borderBox");
		parentDiv.appendChild(newDiv);
    }
	
	createInsertAndHideDiv(parentDiv, classOfDiv, optionalParameters){
		var newDiv = document.createElement("DIV");
		var arrayOfClasses = classOfDiv.split(" ");
		for(var i=0; i<arrayOfClasses.length; i++){
			newDiv.classList.add(arrayOfClasses[i]);
		}
		
		this.hideContentOfDiv(newDiv);
		
		//newDiv.classList.add("mazeBlock");
		newDiv.classList.add("borderBox");
		parentDiv.appendChild(newDiv);
    }
	
	hideContentOfDiv(newDiv){
		//HIDE CONTENT
		newDiv.style.opacity = 0;
		newDiv.classList.add('hiddenContent');
	}
	
	createAndInsertDivWithEffects(parentDiv, classOfDiv, optionalParameters){
		var newDiv = document.createElement("DIV");
		var arrayOfClasses = classOfDiv.split(" ");
		for(var i=0; i<arrayOfClasses.length; i++){
			newDiv.classList.add(arrayOfClasses[i]);
		}
		newDiv.classList.add("mazeBlock");
		newDiv.classList.add("borderBox");
		
		parentDiv.appendChild(newDiv);
    }
	
}

class OptionalParameters {

	constructor(gameMode, fieldSize, borderWidth, gameStyle, idContainer ){
		this.gameMode = gameMode;
		this.fieldSize = fieldSize;
		this.borderWidth = parseInt(borderWidth.replace("px",""));
		this.gameStyle = gameStyle;
		this.idContainer = idContainer;
	}
	
	getGameMode() { return this.gameMode; }
	getFieldSize() { return this.fieldSize; }
	getBorderWidth() { return this.borderWidth; }
	getGameStyle() { return this.gameStyle; }
	getIdOfContainer() { return this.idContainer; }
}


class GameStyle {
	
	constructor(effects, borderColor, backgroundColor, backgroundTextureName){
		this.effects = effects;
		this.borderColor = borderColor;
		this.backgroundColor = backgroundColor;
		this.backgroundTextureName = backgroundTextureName;
	}
	
	getEffects() { return this.effects; }
	getBorderColor() { return this.borderColor; }
	getBackgroundColor() { return this.backgroundColor; }
	getBackgroundTextureName() { return this.backgroundTextureName; }
}

class GameMode {
	
	constructor( gameModeText, visibility, numberVisibleSteps, oneWay, probabilityManyWays, timeLimit ){
		this.gameModeText = gameModeText;
		this.visibility;
		this.numberVisibleSteps;
		this.oneWay;
		this.probabilityManyWays;
		this.timeLimit;
		
		this.applySelectedGameMode(this.gameModeText, visibility, numberVisibleSteps, oneWay, probabilityManyWays, timeLimit);
	}
	
	applySelectedGameMode(gameModeText, visibility, numberVisibleSteps, oneWay, probabilityManyWays, timeLimit) {
		if(gameModeText == "easy"){
			this.visibility = true;
			this.numberVisibleSteps = -1;
			this.oneWay = false;
			this.probabilityManyWays = 0.75;
			this.timeLimit = -1;
		} else if(gameModeText == "normal") {
			this.visibility = true;
			this.numberVisibleSteps = -1;
			this.oneWay = false;
			this.probabilityManyWays = 0.25;
			this.timeLimit = -1;
		} else if(gameModeText == "medium") {
			this.visibility = false;
			this.numberVisibleSteps = 2;
			this.oneWay = true;
			this.probabilityManyWays = 0.0;
			this.timeLimit = -1;
		}  else if(gameModeText == "hard") {
			this.visibility = false;
			this.numberVisibleSteps = 2;
			this.oneWay = true;
			this.probabilityManyWays = 0.0;
			this.timeLimit = -1;
		} else if(gameModeText == "optional") {
			this.visibility = visibility;
			this.numberVisibleSteps = numberVisibleSteps;
			this.oneWay = oneWay;
			this.probabilityManyWays = probabilityManyWays;
			this.timeLimit = timeLimit;
		} else {
			console.log('Undefined mode');
		}
	}
	
	getVisibility() { return this.visibility; }
	getNumberVisibleSteps()  { if( this.visibility == false) { return this.numberVisibleSteps; } else { return -1; } }
	getOneWay() { return this.oneWay; }
	getProbabilityManyWays() { if(this.oneWay == false ){ return this.probabilityManyWays; } else { return 0.0; } }
	getTimeLImit() { return this.timeLimit; }
}

